// Homework_15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

//Declaring a function with parameters. Limit is a maximum numbers where we are searching odds and evens
//x - is a number from which we are starting to search
void FindOddEvenNumbers(int Limit, int x)
{
   
    
    for (int i=x; i<=Limit; i+=2)
    {
        cout << i << " " << endl;
    }
}


int main()
{
    //Declaring and initializing our limit in main function   
    const int Limit = 25;

    //Calling our function with cycle
    cout << "The Odd numbers are: \n";
    FindOddEvenNumbers(Limit, 2);

    cout << "The Even numbers are: \n";
    FindOddEvenNumbers(Limit, 1);

    return 0;
          
}

